import pyodbc
import pprint
import datetime

def connect_db(dsn,server,port,database,user,password):
    conn = None
    try:
        conn = pyodbc.connect("DSN=" + dsn + "; Server=" + server + ";Port=" + port + ";Database=" + database + ";Uid=" + user + ";Pwd=" + password + "")  # Connect to the database
        cur = conn.cursor()  # create a new cursor
        # sql = "INSERT INTO EMPLOYEE (FIRST_NAME,LAST_NAME,AGE,SEX,INCOME) VALUES (cast('Yair' as VARCHAR(20)),cast('Cohav' as VARCHAR(20)),40,cast('F' as VARCHAR(1)),2500)"
        #sql = "INSERT INTO test.\"eyaltest\"(id,name,sex,date_of_birth,city,marital_status,employee_id,smoker,salary,role) " \
        #      "VALUES('{x1}','{x2}','{x3}','{x4}','{x5}','{x6}','{x7}','{x8}','{x9}','{x10}')" \
        #    .format(x1=str(777), x2="Maayan", x3="female", x4=str(datetime.date(year=1977, month=7, day=16)), x5="ashdod",
        #            x6="single", x7="dfh54f", x8=str(True), x9=str(6300), x10="support")
        sql = "UPDATE test.\"eyaltest\" SET '{x0}' = '{x1}'".format(x1="male", x2="female")
        cur.execute(sql)  # Execute the SQL command
        sql = "SELECT * FROM test.eyaltest"
        cur.execute(sql)
        cont = cur.fetchall()  # Commit your changes in the database
        conn.commit()  # commit the changes to the database
        cur.close()  # close communication with the database
    except (Exception, pyodbc.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return cont

def change_values(tbl,columns,old_value,new_value):
    conn = None
    try:
        conn = pyodbc.connect("DSN=PostgreSQL35W; Server=13.92.125.237; Port=5432; Database=dqa; Uid=postgres; Pwd=Ppp@24680")  # Connect to the database
        cur = conn.cursor()  # create a new cursor
        for column in columns:
            set = column + " = '" + new_value + "'"
        sql = "UPDATE {tbl} SET {set}".format(**locals())
        print(sql)
        cur.execute(sql)  # Execute the SQL command
        sql = "SELECT * FROM test.eyaltest"
        cur.execute(sql)
        cont = cur.fetchall()  # Commit your changes in the database
        conn.commit()  # commit the changes to the database
        cur.close()  # close communication with the database
        return cont
    except (Exception, pyodbc.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

if __name__ == '__main__':
    cont = change_values('test.\"eyaltest\"',['sex'],'male','female')
    pprint.pprint(cont)

#"DSN=PostgreSQL35W; Server=13.92.125.237;Port=5432;Database=dqa;Uid=postgres;Pwd=Ppp@24680"